
import React from 'react';
import ReactDOM from 'react-dom';
import App from './../App';
import state from './../state/state'
import { BrowserRouter } from 'react-router-dom';



function rerenderTree (){
    ReactDOM.render(
      <BrowserRouter>
        <App state={state}/>
      </BrowserRouter>,
      document.getElementById('root')
    );
}

  
export  {rerenderTree}