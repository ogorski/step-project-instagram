import Post from './Post/Post';

const Posts = () => {
    return (
        <div>
            <Post nickname="ashley_benson" avatar="https://i.pinimg.com/736x/d6/a9/57/d6a957f1d8045c9c973c12bf5968326f.jpg" image="https://thumbnails.trvl-media.com/KNy1nFVbYxKORvn6fckC5oKZuVI=/773x530/smart/filters:quality(60)/images.trvl-media.com/hotels/17000000/16310000/16304200/16304181/768693a9_z.jpg" caption="Very good view in my life"/>
            <Post nickname="ryan_gosling" avatar="https://freepngimg.com/thumb/ryan_gosling/31253-3-ryan-gosling-transparent-background.png" image="https://media1.fdncms.com/ntslo/imager/u/original/7463932/musicartsculture_movies2-1-f8c4a84633d975d3.jpg" caption="Very good film! It's honor to contrubite in Drive"/>
            <Post nickname="joebiden" avatar="https://upload.wikimedia.org/wikipedia/commons/e/e1/Joe_Biden_official_portrait_2013_%28cropped%29.jpg" image="https://ichef.bbci.co.uk/news/640/cpsprodpb/10243/production/_113951166_index_promo_simple_guide_976_v2.png" caption="Congratulations to Joe Biden! The newly elected President of United States Of America!"/>
            <Post nickname="mary_kelso" avatar="https://images.unsplash.com/photo-1545912452-8aea7e25a3d3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" image="https://d1csarkz8obe9u.cloudfront.net/posterpreviews/vote-biden-for-president-2020-design-template-e296416f3aad76a74c5fcc60ca2bcbac_screen.jpg?ts=1602078064" caption="Going to vote!"/>
            <Post nickname="jay_d" avatar="https://static.wikia.nocookie.net/scrubs/images/0/02/S2-HQ-JD.jpg/revision/latest/scale-to-width-down/340?cb=20090429003501" image="https://www.miga.org/sites/default/files/2018-06/hospital-hallway.jpg" caption="Another day in hospital.."/>
        </div>
    );
}

export default Posts;