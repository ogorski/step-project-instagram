import React, { Component } from 'react';
import s from "./Post.module.scss";

export default class Post extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const nickname = this.props.nickname;
        const avatar = this.props.avatar;
        const caption = this.props.caption;
        const image = this.props.image;

        return <article ref="Post" className={s.post}>
            <header>
                <div className={s.postUser}>
                    <div className={s.postUserAvatar}>
                        <img src={avatar} alt="user avatar" />
                    </div>
                    <div className={s.postUserNickname}>
                        <span>{nickname}</span>
                    </div>
                </div>
            </header>
            <div className={s.postPic}>
                <div className={s.postPicBg}>
                    <img src={image} alt={caption} />
                </div>
            </div>
            <div className={s.postCaption}>
                <strong>{nickname}</strong>{caption}
            </div>
        </article>
    };
};
