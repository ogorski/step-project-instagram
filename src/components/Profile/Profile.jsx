import ProfileInfo from './ProfileInfo/ProfileInfo';
import s from './Profile.module.scss';
import Posts from './Posts/Posts';
import Navbar from './Navbar/Navbar';
import SavedPosts from './SavedPosts/SavedPosts'
import { Route } from 'react-router-dom';

const Profile = (props) => {
    return (
        <div className={s.profileContainer}>
            <div className={s.profileInfoContainer}>
                <ProfileInfo profile={props.profile}/>
            </div>
            <Navbar />
            <Route path='/profile/posts' render={()=> <Posts profile={props.profile}/>}/>
            <Route path='/profile/saved' render={()=> <SavedPosts profile={props.profile}/>}/>
        </div>
    );
}

export default Profile