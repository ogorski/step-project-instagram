import s from './Author.module.scss';
import React from 'react';

const Author = (props) => {
    return (
        <div className={s.authorContainer}>
            <img className={s.avatar} src={props.avatar} />
            <span className={s.nickname}>{props.author}</span>
        </div>
    );
}

export default Author;

