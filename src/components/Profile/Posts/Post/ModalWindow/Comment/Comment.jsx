import s from './Comment.module.scss';
import React from 'react';

const Comment = (props) => {
    return (
        <div className={s.commentBlock}>
                <img className={s.avatar} src={props.avatar} />
            <div className={s.textContainer}>
                <span className={s.nickname}>{props.author}</span>
                <span className={s.message}>{props.comment}</span>
            </div>
        </div>
    );
}

export default Comment;