import s from './InteractionPanel.module.scss';
import React from 'react';

const InteractionPanel = (props) => {
    let likeElement = React.createRef();
    let bookmarkElement = React.createRef();

    const likeClick = () => {
        if (likeElement.current.className === "far fa-heart" + " " + s.iconElement) {
            likeElement.current.className = "fas fa-heart" + " " + s.iconElement;
            props.interaction.like();
        } else if (likeElement.current.className === "fas fa-heart" + " " + s.iconElement) {
            likeElement.current.className = "far fa-heart" + " " + s.iconElement;
            props.interaction.dislike();
        }
    }

    const bookmarkClick = () => {
        if (bookmarkElement.current.className === "far fa-bookmark" + " " + s.iconElement) {
            bookmarkElement.current.className = "fas fa-bookmark" + " " + s.iconElement
            props.interaction.savePost();
        } else if (bookmarkElement.current.className === "fas fa-bookmark" + " " + s.iconElement) {
            bookmarkElement.current.className = "far fa-bookmark" + " " + s.iconElement;
            props.interaction.deletePost();
        }
    }

    return (
        <div className={s.interactionPanel} >
            <i className={"far fa-heart" + " " + s.iconElement}
                ref={likeElement}
                onClick={likeClick}></i>
            <i className={"far fa-bookmark" + " " + s.iconElement}
                ref={bookmarkElement}
                onClick={bookmarkClick}></i>
        </div>
    );
}




export default InteractionPanel;