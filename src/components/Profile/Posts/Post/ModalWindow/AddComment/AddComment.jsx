import s from './AddComment.module.scss';
import React from 'react'

const AddComment = (props) => {
    let newCommentElemnet = React.createRef();
    let addNewComment = () => {
        if(newCommentElemnet.current.value != ''){
            let text = newCommentElemnet.current.value;
            props.comments.addComment(text);
            newCommentElemnet.current.value='';
        }
    }
    return (
        <div className={s.commentContainer}>
            <div className={s.commentText}>
                <textarea className={s.commentTextArea}
                    ref={newCommentElemnet}
                    placeholder='Добавьте комментарий...'></textarea>
            </div>
            <button className={s.btnPost} onClick={addNewComment}>Опубликовать</button>
        </div>
    );
}





export default AddComment;