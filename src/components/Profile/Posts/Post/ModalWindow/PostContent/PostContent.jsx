import s from './PostContent.module.scss';
import React from 'react';


const PostContent = (props) => {
    // debugger;
    return (
        <div className={s.postBackground}>
            <img className={s.postContent} src={props.content} alt="post" />
        </div>
    );
}

export default PostContent;
