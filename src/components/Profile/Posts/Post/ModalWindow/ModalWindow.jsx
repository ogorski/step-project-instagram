import s from './ModalWindow.module.scss';
import AddComment from './AddComment/AddComment';
import InteractionPanel from './InteractionPanel/InteractionPanel';
import Comment from './Comment/Comment';
import Author from './Author/Author';
import PostContent from "./PostContent/PostContent";
import React from 'react';

const ModalWindow = (props) => {
    let newCommentElemnet = props.postInfo.comments.map(p=><Comment author={p.author} avatar={p.avatar} comment={p.comment}/>)
    return (
        <div className={s.myModal} className={s.modal} ref={newPostElemnet} onClick={closeModal}>
            <div className={s.modalContent}>
                <div className={s.modalContentContainer}>
                        <PostContent content={props.content}/>
                    <div className={s.modalCommentsContainer}>
                        <Author author={props.postInfo.author} 
                                avatar={props.postInfo.avatar}/>
                    <div className={s.commentArea}>
                        { newCommentElemnet }
                    </div>
                        <InteractionPanel interaction={props.postInfo}/>
                        <AddComment comments={props.postInfo}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

let newPostElemnet = React.createRef();
const openModal = () => {
    newPostElemnet.current.style.display = 'block'
}
const closeModal = (event) => {
    if (event.target === newPostElemnet.current) {
        newPostElemnet.current.style.display = "none";
    }
}
export { openModal };

export default ModalWindow;