import ModalWindow, { openModal } from './ModalWindow/ModalWindow';
import React from 'react'
import s from './Post.module.scss';

const Post = (props) => {
    return (
        <div className={s.postBlock}>
            <ModalWindow postInfo={props.postInfo} content={props.content}/>
            <div className={s.post} onClick={openModal} >
                <div className={s.postInfoContainer}>
                    <div className={s.postInfo}>
                        <span className={s.postData}><i className="fa fa-heart"></i> {props.postInfo.likeCount}</span>
                        <span className={s.postData}><i className="fa fa-comment"></i> {props.postInfo.commentCount()}</span>
                    </div>
                </div>
                <div className={s.postImageContainer}>
                    <img className={s.postImage} src={props.content} alt="" />
                </div>
            </div>
        </div>
    );
}

// let postModal = React.createRef();




export default Post;
