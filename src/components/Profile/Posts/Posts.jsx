import s from './Posts.module.scss';
import react from 'react'
import Post from './Post/Post'


const Posts = (props) => {
    let postElement = props.profile.posts.map(p=><Post postInfo={p} content={p.postContent}/>);
    return (
        <div className={s.postsContainer}>
            { postElement }
        </div>
    );
}


export default Posts;