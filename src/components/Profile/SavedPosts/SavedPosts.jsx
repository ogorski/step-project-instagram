import s from './SavedPosts.module.scss';
import Post from './../Posts/Post/Post'

const SavedPosts = (props) => {
    let savedPostElement = props.profile.savedPosts.map(p=><Post postInfo={p} content={p.postContent}/>);
    return (
        <div className={s.savedPostsContainer}>
            {savedPostElement}
        </div>
    );
}

export default SavedPosts;