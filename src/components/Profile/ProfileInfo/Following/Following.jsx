import react from 'react';
import s from './Following.module.scss';

const Following = (props) => {
    return (
        <span className={s.profileInfo}>
            <b>{props.profile.following} </b>
                подписок
        </span>
    );
}

export default Following;
