import react from 'react';
import s from './Followers.module.scss';

const Followers = (props) => {
    return (
        <span className={s.profileInfo}>
            <b>{props.profile.followers} </b>
        подписчиков
        </span>
    );
}

export default Followers;