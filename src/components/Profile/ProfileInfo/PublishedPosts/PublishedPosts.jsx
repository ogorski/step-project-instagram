import react from 'react';
import s from './PublishedPosts.module.scss';

const PublishedPosts = (props) => {
    return (
        <span className={s.profileInfo}>
            <b>{props.profile.publishedPosts()} </b>
            публикаций
        </span>
    );
}

export default PublishedPosts;