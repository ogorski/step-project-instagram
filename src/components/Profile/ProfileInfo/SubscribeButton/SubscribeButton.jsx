import React from 'react';
import s from './SubscribeButton.module.scss';

let subscribed =false;

const SubscribeButton = (props) => {
    let subscribeBtn = React.createRef();
    
    let subscribe = () => {
        if(subscribed == false){
            subscribeBtn.current.style="background:none; color:#262626;";
            subscribeBtn.current.innerHTML="Отписаться"
            props.profile.subscribe();
            subscribed = true
        } else if (subscribed == true){
            subscribeBtn.current.style="background:#0095f6; color:#fff;";
            subscribeBtn.current.innerHTML="Подписаться"
            props.profile.unsubscribe();
            subscribed = false
        }
    }
    return (
        <div className={s.subscribeButtonContainer}>
            <button className={s.subscribeButton} onClick={subscribe} ref={subscribeBtn}>Подписаться</button>
        </div>
    );
}

export default SubscribeButton;

