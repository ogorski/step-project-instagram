import s from './ProfileInfo.module.scss';
import PublishedPosts from './PublishedPosts/PublishedPosts'
import Followers from "./Followers/Followers";
import Following from "./Following/Following";
import SubscribeButton from "./SubscribeButton/SubscribeButton"

const ProfileInfo = (props) => {    
    return (
        <div className={s.profileInfoContainer} >
            <div className={s.avatarContainer}>
                <img className={s.avatar} src={props.profile.avatar} />
            </div>
            <div>
                <div className={s.profileContainer}>
                    <div className={s.nickname}>
                        {props.profile.nickname}
                    </div>
                    <SubscribeButton profile={props.profile}/>
                </div>
                <div>
                    <ul className={s.profileInfoCounters}>
                        <li className={s.profileInfoCounter}>
                            <PublishedPosts profile={props.profile}/>
                        </li>
                        <li className={s.profileInfoCounter}>
                            <Followers profile={props.profile}/>
                        </li>
                        <li className={s.profileInfoCounter}>
                            <Following profile={props.profile}/>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default ProfileInfo;