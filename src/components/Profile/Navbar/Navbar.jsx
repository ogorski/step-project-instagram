import s from './Navbar.module.scss'
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <div className={s.navbarListContainer}>
            <ul className={s.navbarList}>
                <li className={s.navbarItem}>
                    <NavLink to='/profile/posts' 
                        className={s.navbarLink} 
                        activeClassName={s.active}>
                        Публикации
                    </NavLink>
                </li>
                <li className={s.navbarItem}>
                    <NavLink to='/profile/saved'
                        className={s.navbarLink} 
                        activeClassName={s.active}>
                        Сохраненное
                    </NavLink>
                </li>
            </ul>
        </div>
    );
}

export default Navbar;