import React from "react";
import s from './Header.module.scss'
import { NavLink } from 'react-router-dom';

const Header = (props) => {
  return (
    <nav className={s.nav}>
      <div className={s.navMenus}>
        <div className={s.navBrand}>
          <NavLink className={s.navBrandLogo} to="/"></NavLink>
        </div>
        <div className={s.navbarIcons}>
          <NavLink to='/profile/posts' className={s.navbarDirectIcon}>
            <img className={s.avatar} src={props.profile.avatar} alt="" />
          </NavLink>
        </div>
      </div>
    </nav>
  );
}
export default Header;