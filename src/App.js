import React from "react";
import Header from "./components/Header/Header";
import s from './App.module.scss';
import Profile from './components/Profile/Profile'
import { Route } from 'react-router-dom';
import Posts from './components/Posts/Posts'

const App = (props) => {
  return (
      <div className={s.mainBackground}>
        <Header profile={props.state.profile}/>
        <div className={s.postContainer}>
          <Route path='/profile/' render={()=> <Profile 
            profile={props.state.profile}/>}/>
          <Route exact path='/' render={()=> <Posts/>}/>
        </div>
      </div>
  );
}

export default App;
