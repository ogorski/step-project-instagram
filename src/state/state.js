import AddComment from "../components/Profile/Posts/Post/ModalWindow/AddComment/AddComment";
import {rerenderTree} from './../render/render';

let state = {
    profile:{
        avatar: "https://cdn3.iconfinder.com/data/icons/basic-icon-set/32/test-14-512.png",
        nickname:"NickTest",
        followers:0,
        following:0,
        savedPosts:[],
        publishedPosts:function(){
            return this.posts.length;
        },
        posts:[],
        subscribe:function(){
            this.followers ++;
            rerenderTree();
        },
        unsubscribe:function(){
            this.followers --;
            rerenderTree();
        },
        addPost:function(postContent){
        this.posts.push(this.createPost(postContent))
        },
        createPost:function(content){
        return {
            author: this.nickname, 
            avatar: this.avatar,
            postContent:content,
            likeCount:0,
            commentCount:function(){
                return this.comments.length;
            },
            comments:[],
            addComment:function(postComment){
                let newComment = {
                    author:this.author,
                    avatar:this.avatar,
                    comment:postComment,
                }; 
                this.comments.push(newComment);
                rerenderTree();
            },
            like:function() {
                this.likeCount ++;
                rerenderTree();
            },
            dislike:function() {
                this.likeCount --;
                rerenderTree();
            },
            savePost:function() {
                state.profile.savedPosts.push(this);
                rerenderTree();
            },
            deletePost:function(){
                console.log(state.profile.savedPosts.indexOf(this))
                state.profile.savedPosts.splice(state.profile.savedPosts.indexOf(this),1);
                rerenderTree();
            }
        }
    }
         
}
}

  

state.profile.addPost('https://cdn.vox-cdn.com/thumbor/3NtkqgKUmPjgyUuPN7PPp5CGVMM=/0x0:4096x2645/1200x800/filters:focal(1721x996:2375x1650)/cdn.vox-cdn.com/uploads/chorus_image/image/67225990/Suggested_Posts_in_Feed.0.png');

export default state;